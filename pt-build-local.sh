#!/bin/bash


build_image () {
	REPO=$1
	DOCKERFILE=$2
	TAG=$3
	cd $REPO
	docker build -t $TAG -f $DOCKERFILE .
}

# export variables from .rq_vars
source .rq_vars
# Build RQ project local images
COMMAND_LIST="all|rq-be|rq-nginx"
case $1 in 
	"all")
	  echo "Build RQ local stand:"
	  echo "Build rq-be:local image:"
	  build_image "${RQ_BE_REPO}" "Dockerfile" "rq-be:local"
	  
	  echo "Build rq-ui-proposal:local image:"
	  cd ${RQ_UI_PROPOSAL}
	  git checkout ${RQ_UI_PROPOSAL_BRANCH}
	  build_image "${RQ_UI_PROPOSAL}" "Dockerfile" "rq-ui-proposal:local"
	  
	  echo "Build rq-ui-quote:local image:"
	  cd ${RQ_UI_QUOTE}
	  git checkout ${RQ_UI_QUOTE}
	  build_image "${RQ_UI_QUOTE}" "Dockerfile" "rq-ui-quote:local"
	  
	  echo "Build rq-nginx:local image:"
	  build_image ${RQ_NGINX_REPO} "Dockerfile" "rq-be:local"
	  ;;
	"rq-be")
	  echo "Build rq-be:local image:"
	  build_image ${RQ_BE_REPO} "Dockerfile" "rq-be:local"
	  ;;
	"rq-nginx")
	  echo "Build rq-nginx:local (include rq-ui-quote:local and rq-ui-proposal:local)"
	  echo "Build rq-ui-proposal:local image:"
	  build_image "${RQ_UI_PROPOSAL}" "Dockerfile" "rq-ui-proposal:local"
	  echo "Build rq-ui-quote:local image:"
	  build_image "${RQ_UI_QUOTE}" "Dockerfile" "rq-ui-quote:local"
	  echo "Build rq-nginx:local image:"
	  build_image "${RQ_NGINX_REPO}" "Dockerfile" "rq-be:local"		
	  ;;
	"help")
	  echo "Usage ./rq-build-local <param>"
	  echo "available params:"
	  echo "[$COMMAND_LIST]"
	  ;;
esac
	