#!/bin/bash


build_image () {
	REPO=$1
	DOCKERFILE=$2
	TAG=$3
	cd $REPO
	docker build -t $TAG -f $DOCKERFILE .
}

# export variables from .rq_vars
source .rq_vars
# Build RQ project local images
COMMAND_LIST="all|rq-be|rq-nginx"
case $1 in 
	"all")
	  printf '*%.0s' {1..80} && echo
	  echo "Build RQ local stand:"
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-be:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image "${RQ_BE_REPO}" "Dockerfile" "rq-be:local"
  
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-ui-proposal:local image:"
	  printf '#%.0s' {1..50} && echo
	  cd ${RQ_UI_REPO}
	  git checkout ${RQ_UI_PROPOSAL_BRANCH}
	  build_image "${RQ_UI_REPO}" "Dockerfile" "rq-ui-proposal:local"
	  
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-ui-quote:local image:"
	  printf '#%.0s' {1..50} && echo
	  cd ${RQ_UI_REPO}
	  git checkout ${RQ_UI_QUOTE_BRANCH}
	  build_image "${RQ_UI_REPO}" "Dockerfile" "rq-ui-quote:local"
	  
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-nginx:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image ${RQ_NGINX_REPO} "Dockerfile" "rq-be:local"
	  ;;
	"rq-be")
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-be:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image ${RQ_BE_REPO} "Dockerfile" "rq-be:local"
	  ;;
	"rq-nginx")
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-nginx:local (include rq-ui-quote:local and rq-ui-proposal:local)"
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-ui-proposal:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image "${RQ_UI_PROPOSAL}" "Dockerfile" "rq-ui-proposal:local"
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-ui-quote:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image "${RQ_UI_QUOTE}" "Dockerfile" "rq-ui-quote:local"
	  printf '#%.0s' {1..50} && echo
	  echo "Build rq-nginx:local image:"
	  printf '#%.0s' {1..50} && echo
	  build_image "${RQ_NGINX_REPO}" "Dockerfile" "rq-be:local"		
	  ;;
	"help")
	  printf '*%.0s' {1..80} && echo
	  echo "Usage ./rq-build-local <param>"
	  echo "available params:"
	  echo "[$COMMAND_LIST]"
	  ;;
esac
	