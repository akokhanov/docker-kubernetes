#!/bin/bash

# Uninstall brew
#echo "Uninstall brew (if necessary)...."
#/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# Uninstall wget
echo "Uninstall wget ...."
brew install wget
###
# install minikube
# Minikube requires that VT-x/AMD-v virtualization is enabled in BIOS. Check this
echo "Uninstall kubectl..."
brew uninstall kubectl
echo "Uninstall docker...."
brew cask uninstall docker
echo "Uninstall minikube...."
brew cask uninstall minikube
echo "Uninstall virtualbox...."
brew cask uninstall virtualbox
brew cask uninstall virtualbox-extension-pack
