#!/bin/bash

# Install brew
echo "Install brew (if necessary)...."
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# Install wget
echo "Install wget ...."
brew install wget
###
# install minikube
# Minikube requires that VT-x/AMD-v virtualization is enabled in BIOS. Check this
echo "Minikube requires that VT-x/AMD-v virtualization is enabled in BIOS. Id there are output - all OK."
sysctl -a | grep machdep.cpu.features | grep VMX
echo "Done"
echo "Update brew...."
brew update
echo "Install kubectl..."
brew install kubectl
echo "Install docker...."
brew cask install docker
echo "Install minikube...."
brew cask install minikube

####
#minikube stop
#minikube delete
#brew cask uninstall --force minikube
#brew cask edit minikube
#See below for file
#brew cask install minikube
##########3

#brew cask install minikube --version=0.25.2
echo "Install virtualbox...."
brew cask install virtualbox
brew cask install virtualbox-extension-pack
echo "======================="
echo "INSTALLED VERSIONS:"
echo "======================="
docker --version
docker-compose --version
docker-machine --version
minikube version
kubectl version --client
echo "Minikube starting...."
minikube start



# install kubectl, docker, minikube, virtualbox
#brew update && brew install kubectl && brew cask install docker minikube virtualbox

# 
