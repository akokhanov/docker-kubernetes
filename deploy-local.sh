#!/bin/bash


build_image () {
	REPO=$1
	DOCKERFILE=$2
	TAG=$3
	cd $REPO
	docker build -t $TAG -f $DOCKERFILE .
}

# export variables from .rq_vars
source .rq_vars
# Build RQ project local images
COMMAND_LIST="all|rq|pt"
case $1 in 
	"all")
	  printf '#%.0s' {1..80} && echo
	  echo " # DEPLOY RQ LOCAL STAND"		
	  printf '#%.0s' {1..80} && echo
	  echo "Deploy RQ DB "
	  printf '-%.0s' {1..50} && echo
	  printf '-%.0s' {1..50} && echo
	  echo "Deploy rq-be:"
	  printf '-%.0s' {1..50} && echo  
	  printf '-%.0s' {1..50} && echo
	  echo " Deploy rq-nginx:"
	  printf '-%.0s' {1..50} && echo
	 
	  printf '#%.0s' {1..80} && echo
	  echo "# DEPLOY PT LOCAL STAND"		
	  printf '#%.0s' {1..80} && echo
	  echo "Deploy pt-db:"
	  printf '#%.0s' {1..50} && echo
	  printf '#%.0s' {1..50} && echo
	  ;;
	"rq")
	  printf '#%.0s' {1..80} && echo
	  echo " # DEPLOY RQ LOCAL STAND"		
	  printf '#%.0s' {1..80} && echo
	  echo "Deploy RQ DB "
	  printf '-%.0s' {1..50} && echo
	  printf '-%.0s' {1..50} && echo
	  echo "Deploy rq-be:"
	  printf '-%.0s' {1..50} && echo  
	  printf '-%.0s' {1..50} && echo
	  echo " Deploy rq-nginx:"
	  printf '-%.0s' {1..50} && echo	
	  ;;
	"pt")
	  ;;
	"help")
	  printf '*%.0s' {1..80} && echo
	  echo "Usage ./rq-build-local <param>"
	  echo "available params:"
	  echo "[$COMMAND_LIST]"
	  ;;
esac
	